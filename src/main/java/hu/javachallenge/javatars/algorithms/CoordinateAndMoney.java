package hu.javachallenge.javatars.algorithms;

import hu.javachallenge.javatars.util.Coordinate;

public class CoordinateAndMoney {
    
    private Coordinate coordinate;
    private int money;

    public CoordinateAndMoney(Coordinate coordinate, int money) {
        this.coordinate = coordinate;
        this.money = money;
    }

    public Coordinate getCoordinate() {
        return coordinate;
    }

    public void setCoordinate(Coordinate coordinate) {
        this.coordinate = coordinate;
    }

    public int getMoney() {
        return money;
    }

    public void setMoney(int money) {
        this.money = money;
    }
    
}

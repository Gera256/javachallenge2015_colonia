package hu.javachallenge.javatars.util;

import eu.loxon.centralcontrol.ObjectType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CellInformation {
    
    private static final Logger LOGGER = LoggerFactory.getLogger(CellInformation.class);

	private ObjectType type;
	private boolean enemy = false;
	
	public CellInformation(ObjectType type){
		this.type = type;
	}
	
	public CellInformation(ObjectType type, String team){
		this.type = type;
		
		if(team != null && !(team.equals("javatars"))){
			enemy = true;
		}
	}

	public ObjectType getType() {
		return type;
	}

	public void setType(ObjectType type) {
		this.type = type;
	}

	public boolean isEnemy() {
		return enemy;
	}

	public void setEnemy(boolean enemy) {
		this.enemy = enemy;
	}
	
	
	
}
